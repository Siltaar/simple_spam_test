#!/usr/bin/python3 -O
##!/usr/bin/python2 -O
# coding: utf-8
# author : Simon Descarpentries
# date: 2017 - 2018
# licence: GPLv3

from doctest import run_docstring_examples
from datetime import datetime
from simple_spam_test import spam_test
from test_simple_spam_test import test_spam_test_theoritical_cases, test_spam_test_real_cases

"""

20240422 3 : 29 tests : 2,45s ; 84,48ms/t

Debian 12.5
Python 3.11.2

20240202 3 : 28 tests : 2,80s ; 100ms/t
20240130 3 : 31 tests ; 8.41s ; pypy 7.3.5 / Python 3.7.10
20240130 2 : 31 tests ; 5.61s ; pypy 7.3.3 / Python 2.7
20240130 3 : 31 tests ; 3.38s ; in production mode
20240130 2 : 31 tests ; 2.99s ; in production mode
20240129 3 : 31 tests ; 5.1s ; in debug mode
20240129 2 : 31 tests ; 6.8s ; in debug mode
20220107 3 : 31 tests ; 3.37s ; 108,7ms/t (System upgrades :  ~8% faster ; -11.9%)
20220107 2 : 31 tests ; 2.97s ; 95,8ms/t (System upgrades : ~13% faster)

Python 3.9.2
Python 2.7.18
Debian 11.2

20180823 3 : 31 tests ; 3.65s ; 117,7ms/t
20180823 2 : 31 tests ; 3.22s ; 110,3ms/t (catch UnicodeDecodeError)
20180720 3 : 30 tests ; 3.55s ; 118,3ms/t
20180720 2 : 30 tests ; 3.08s ; 102,6ms/t (test for missing unsubscribe link)
20180719 3 : 29 tests ; 3.32s ; 111,4ms/t
20180719 2 : 29 tests ; 2.87s ; 098,9ms/t (big txt, gradation of alpha_len tests)
20180619 3 : 28 tests ; 2.99s ; 106,7ms/t  - 10.5%
20180619 2 : 28 tests ; 2.68s ; 095,7ms/t (RE strip HTML if no body)
20180523 3 : 28 tests ; 2.68s ; 095,7ms/t  - 13.5%
20180523 2 : 28 tests ; 2.32s ; 082,8ms/t (one more real testcase)
20180504 3 : 27 tests ; 2.51s ; 092,9ms/t  - 13.5%
20180504 2 : 27 tests ; 2.17s ; 080,3ms/t (stop counting recipients)
20180430 3 : 28 tests ; 2.72s ; 097,1ms/t  - 13.5%
20180430 2 : 28 tests ; 2.35s ; 083,9ms/t (regexp to count links, if >4 same domain -> +1)
20180328 3 : 27 tests ; 2.43s ; 090,0ms/t  - 12.5%
20180328 2 : 27 tests ; 2.13s ; 078,8ms/t (regexp out badchars ; seeks bad HTML in 10 1st char)
20180326 3 : 26 tests ; 2.32s ; 089,2ms/t  - 12%
20180326 2 : 26 tests ; 2.05s ; 078,8ms/t (spam if body len < 25 and no HTML part)
20180324 3 :  8 tests ; 1.440 ; 180,0ms/t  - 15%
20180324 2 :  8 tests ; 1.230 ; 153,7ms/t (real cases only)
20180324 3 : 17 tests ; 0.797 ; 046,8ms/t  - 8%
20180324 2 : 17 tests ; 0.740 ; 043,5ms/t (theoritical cases only)
20180322 3 : 25 tests ; 2.20s ; 088,0ms/t  - 13%
20180322 2 : 25 tests ; 1,92s ; 076,8ms/t (body alpha len > 128 ; HTML < 30kc)
20180321 3 : 24 tests ; 2.01s ; 083,7ms/t
20180321 2 : 24 tests ; 1,77s ; 073,7ms/t (separate tests in new file ; slower real testcases..)
20180315 3 : 23 tests ; 1,87s ; 081,3ms/t
20180315 2 : 23 tests ; 1,65s ; 071,7ms/t (no more 'no text' as body len already checked)
20180314 3 : 23 tests ; 1,92s ; 083,4ms/t
20180314 2 : 23 tests ; 1,70s ; 073,9ms/t (check big HTML, no text, small text...)
20180130 3 : 23 tests ; 1,87s ; 081,3ms/t
20180130 2 : 23 tests ; 1,64s ; 071,3ms/t (check bad HTML)
20180112 3 : 22 tests ; 1,74s ; 079,0ms/t
20180112 2 : 22 tests ; 1,53s ; 069,5ms/t (check isalpha instead of isalnum)

Python 3.5.3
Python 2.7.13
Debian 9.3
Linux 4.9.0-5-amd64 #1 SMP Debian 4.9.65-3+deb9u2 (2018-01-04) x86_64 GNU/Linux


20171220 3 : 22 tests ; 1,68s ; 076,3ms/t
20171220 2 : 22 tests ; 1,48s ; 067,2ms/t (check body wild len against alnum)
20171201 3 : 21 tests ; 1,33s ; 063,0ms/t
20171201 2 : 21 tests ; 1,15s ; 054,7ms/t (accept pgp-encrypted stream as valid)
20171124 3 : 20 tests ; 1.20s ; 060,0ms/t
20171124 2 : 20 tests ; 1.06s ; 053,0ms/t (check alnum characters)
20171012 3 : 19 tests ; 0.99s ; 052,1ms/t
20171012 2 : 19 tests ; 0.86s ; 045,2ms/t (check body readability)
20171012 3 : 19 tests ; 0.90s ; 047,3ms/t
20171012 2 : 19 tests ; 0.80s ; 042,1ms/t (parse all mail)
20171012 3 : 19 tests ; 0.81s ; 042,6ms/t
20171012 2 : 19 tests ; 0.71s ; 037,3ms/t (added 2 real cases)
20171012 3 : 17 tests ; 0.62s ; 036,4ms/t

Commande : python -O time_simple_spam_test.py
Python 3.4.2
Python 2.7.9
Debian 8.9
Intel(R) Xeon(R) CPU           L5420  @ 2.50GHz

"""


startTime = datetime.now()

for i in range(0, 100):
	run_docstring_examples(test_spam_test_theoritical_cases, globals())
	run_docstring_examples(test_spam_test_real_cases, globals())

print(datetime.now() - startTime)
