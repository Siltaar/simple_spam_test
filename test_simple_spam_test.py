#!/usr/bin/env python3
# coding: utf-8
# author : Simon Descarpentries
# date: 2017 - 2018
# licence: GPLv3

from doctest import run_docstring_examples
from simple_spam_test import spam_test


def test_spam_test_theoritical_cases(stdin_eml):
	"""
	>>> spam_test('From:Bb<b@b.tk>\\nTo:a@a.tk\\nSubject:eml ok\\n'
	... 'Date:Wed, 26 Apr 2017 16:20:14 +0200\\nReceived:Wed, 26 Apr 2017 16:21:14 +0200\\n'
	... 'Avec un contenu suffisamment long ça devrait aller')
	0
	>>> spam_test('To:\\nSubject: Missing From and To should be scored 2\\n'
	... 'Date:Wed, 26 Apr 2017 16:20:14 +0200\\nReceived:Wed, 26 Apr 2017 16:21:14 +0200')
	1
	>>> spam_test('To:a@a.tk\\nSubject:/// Not / ASCII //////\\n'
	... 'Date:Wed, 26 Apr 2017 16:20:14 +0200\\nReceived:Wed, 26 Apr 2017 16:21:14 +0200')
	1
	>>> spam_test('To:No subject scored 2 <a@a.tk>\\n'
	... 'Date:Wed, 26 Apr 2017 16:20:14 +0200\\nReceived:Wed, 26 Apr 2017 16:21:14 +0200')
	2
	>>> spam_test('Subject: =?gb2312?B?vNLT0NChxau499bW1sa3/sC009W78w==?=\\n'
	... 'Date:Wed, 26 Apr 2017 16:20:14 +0200\\nReceived:Wed, 26 Apr 2017 16:21:14 +0200')
	2
	>>> spam_test('Subject: =?gb2312?B?Encoding error score 2 代 =?=\\n'
	... 'Date:Wed, 26 Apr 2017 16:20:14 +0200\\nReceived:Wed, 26 Apr 2017 16:21:14 +0200')
	1
	>>> spam_test('Subject: Near past +1 d \\nDate: Wed, 26 Apr 2017 16:20:14 +0200\\n'
	... 'Received:Wed, 27 Apr 2017 22:21:14 +0200')
	2
	>>> spam_test('Subject: Near futur -2 h\\nDate: Wed, 26 Apr 2017 16:20:14 +0200\\n'
	... 'Received:Wed, 26 Apr 2017 14:19:14 +0200')
	2
	>>> spam_test('Subject: Far past +15 d \\nDate: Tue, 10 Apr 2017 16:20:14 +0200\\n'
	... 'Received:Wed, 26 Apr 2017 14:21:14 +0200')
	3
	>>> spam_test('Subject: Far futur -2 d \\nDate: Wed, 26 Apr 2017 16:20:14 +0200\\n'
	... 'Received:Mon, 24 Apr 2017 16:19:14 +0200')
	3
	>>> spam_test('From: =?utf-8?b?5Luj?= <a@a.tk>\\nDate: Wed, 26 Apr '
	... '2017 16:20:14 +0200\\nReceived:Wed, 26 Apr 2017 16:25:14 +0200')
	4
	>>> spam_test('X-Spam-Status: Yes')
	5
	>>> spam_test('X-Spam-Level: ***')
	5
	"""
	# >>> spam_test('To:a@a.tk,b@b.tk,c@c.tk,d@d.tk,e@e.tk,f@f.tk,g@g.tk,'
	# ... 'h@h.tk,i@i.tk,j@j.tk\\nSubject: More than 9 recipients, scored 2\\n'
	# ...'Date:Wed, 26 Apr 2017 16:20:14 +0200\\nReceived:Wed, 26 Apr 2017 16:21:14 +0200')
	# 3
	return spam_test(stdin_eml)


def test_spam_test_real_cases(stdin_eml):
	"""
	>>> spam_test(open('email_test/20171010.eml').read())  # chinese content
	3
	>>> spam_test(open('email_test/20171012.eml').read())  # no text nor HTML part
	2
	>>> spam_test(open('email_test/20171107.eml').read())  # longer chinese content
	4
	>>> spam_test(open('email_test/20171130.eml').read())  # PGP ciphered email
	0
	>>> spam_test(open('email_test/20171219.eml').read())  # chinese base64 body
	5
	>>> spam_test(open('email_test/20180130.eml').read())  # no text, bad HTML
	0
	>>> spam_test(open('email_test/20180321.eml').read())  # small body chinese Subj, To:
	3
	>>> spam_test(open('email_test/20180322.eml').read())  # valid big HTML body only
	1
	>>> spam_test(open('email_test/20180326.eml').read())  # small text 0 alpha length
	1
	>>> spam_test(open('email_test/20180328.eml').read())  # Mozilla AsciiArt deco
	0
	>>> spam_test(open('email_test/20180429.eml').read())  # Too much same-domain links
	1
	>>> spam_test(open('email_test/20180523.eml').read())  # Other same-domain links limits
	1
	>>> spam_test(open('email_test/20180718.eml').read())  # Chinese content, big body
	2
	>>> spam_test(open('email_test/20180720.eml').read())  # No link to unsubscribe
	2
	>>> spam_test(open('email_test/20180823.eml').read())  # UnicodeDecodeError Subject:
	0
	>>> spam_test(open('email_test/20240416.eml').read())  # Tiny body : but why ?
	0
	"""
	return spam_test(stdin_eml)


if __name__ == "__main__":
	run_docstring_examples(test_spam_test_theoritical_cases, globals())
	run_docstring_examples(test_spam_test_real_cases, globals())
