from simple_spam_test import spam_test_eml_log
from io import TextIOWrapper

import sys
# https://mypyc.readthedocs.io/en/latest/performance_tips_and_tricks.html#performance-tips
# import gc
# import os
# gc.set_threshold(150000)

eml = None
log = ''
score, eml, log = spam_test_eml_log(TextIOWrapper(sys.stdin.buffer, errors='ignore').read())
eml['x-simple-spam-score'] = str(score)
eml['x-simple-spam-log'] = log
print(eml)
# os._exit(0)
