#!/usr/bin/env python3
# coding: utf-8
# author : Simon Descarpentries
# date: 2017 - 2018
# licence: GPLv3

from email.parser import Parser
from email.header import decode_header, make_header
from email.utils import getaddresses
from email.utils import parseaddr, parsedate_tz, mktime_tz
from datetime import datetime, timedelta
from re import DOTALL, IGNORECASE, compile as compile_re, Pattern

import typing
import email
import email.message
import sys

purge_html_re = compile_re(  # match, to remove :
	r'<(sty|(o|w):|scr|tit|[^y]*y\s*:\s*(n|h)).*?</[^>]*'
	# style, o: w:, script, title, display:none / hidden HTML tags, text leaf, partial ending
	'|<!--.*?-->'  # HTML comments
	'|<[^>]*'	  # all complete HTML tags,  # some may be cut at the end/begining
	'|&[^;]*;'	  # HTML entities
	r'|[\d*]'	  # links prefix in converted texts
	r'|[^\s<\xc2\xa0]{26,}'  # - chunks of symbols without spaces too big to be words (as URL)
	r'|[\n\xc2\xa0\t\r\f\v\\> ]',
	DOTALL)


def spam_test(stdin_eml: str) -> int:
	score, eml, log = spam_test_eml_log(stdin_eml)
	return score


def spam_test_eml_log(stdin_eml: str) -> tuple[int, email.message.Message, str]:
	eml = Parser().parsestr(stdin_eml)
	score = 0
	log = ''
	put("%s " % eml.get('Subject', '')[:20].ljust(20))
	ctype = ''
	text_parts = []
	html_parts = []
	main_html_src = ''
	same_links = True
	for part in eml.walk():
		ctype = part.get_content_type()
		# put('ctype %s ' % part.get_content_type())
		# needed to avoid attached PDF used as body
		if 'plain' in ctype or \
			('application/octet-stream' in ctype and (
				part.get_filename() and 'pdf' not in str(part.get_filename()))):
			text_parts.append(part)
		if 'html' in ctype:
			html_parts.append(part)
	for part in html_parts:
		html_src = str(part.get_payload(decode=True))
		if len(main_html_src) < len(html_src):
			main_html_src = html_src
		"""
		if b'<' not in html_src[:10]:  # looks like malformed HTML
			score += 1
			log += 'bad HTML '
			put(yel("bad HTML "))
		"""
		if len(html_src) > 60000:
			put(yel("big HTML "))
			log += 'big HTML '
			score += 1
		if same_links:
			a = max_same_links(html_src, htm_links_re)
			ln_score, ln_log = same_links_score(a, 'HTML')
			score, log = score + ln_score, log + ln_log
			if ln_score:
				same_links = False
	body = ''
	for part in text_parts:
		# put('part ' + str(part))
		text = part.get_payload(decode=True)
		# put('text ' + str(text))
		if len(body) < len(text):
			# body = text
			try:
				body = str(text, 'utf-8')
			except UnicodeDecodeError as e:
				put(f"exc <UnicodeDecodeError> {str(e)} \n")
				body = str(text)
				# put('body ' + body)
	body_len = len(body)
	if body_len == 0:
		body = main_html_src[:30000]
		body = purge_html_re.sub('', body)
	elif body_len > 25000:
		score += 1
		log += 'big raw body %i ' % body_len
		put(yel("big") + " raw " + yel("body") + " %i " % body_len)
	else:
		shorten_body = str(body[-999:])
		if txt_linkwords_re.search(shorten_body) and not txt_links_re.search(shorten_body):
			score += 1
			log += 'missing link '
			put(yel("missing") + " link ")
		# bw_score, bw_log = too_big_words_score(body)  # can't accept PGP ciphered e-mails
		# score, log = score + bw_score, log + bw_log
	body_len, body_alpha_len = email_alpha_len(body, lambda b: b[:256])
	body_score, body_log = txt_alpha_len(body_len, body_alpha_len, 'body', 0)
	score, log = score + body_score, log + body_log
	if same_links:
		a = max_same_links(body, txt_links_re)
		ln_score, ln_log = same_links_score(a, 'TXT')
		score, log = score + ln_score, log + ln_log
	subj_len, subj_alpha_len = email_alpha_len(str(eml.get('Subject', '')), header_str)
	subj_score, subj_log = txt_alpha_len(subj_len, subj_alpha_len, 'subj', 0)
	score, log = score + subj_score, log + subj_log
	from_len, from_alpha_len = email_alpha_len(str(parseaddr(eml.get('From', ''))[0]), header_str)
	if from_len > 0:
		from_score, from_log = txt_alpha_len(from_len, from_alpha_len, 'from', 5)
		score, log = score + from_score, log + from_log
	recipient_count = len(getaddresses(eml.get_all('To', []) + eml.get_all('Cc', [])))
	# if recipient_count == 0 or recipient_count > 9:  # too much false positive
	if recipient_count > 30:  # If there is too much recipients, it may be a spam
		score += 1
		put(yel("recs") + " %i " % (recipient_count))
		log += 'recs %i ' % recipient_count
	recv_tz = parsedate_tz(eml.get('Received', 'Sat, 01 Jan 9999 01:01:01 +0000')[-30:])
	assert recv_tz is not None
	recv_dt = datetime.utcfromtimestamp(mktime_tz(recv_tz))
	eml_tz = parsedate_tz(eml.get('Date', 'Sat, 01 Jan 0001 01:01:01 +0000'))
	assert eml_tz is not None
	eml_dt = datetime.utcfromtimestamp(mktime_tz(eml_tz))
	if eml_dt < recv_dt - timedelta(days=1) or eml_dt > recv_dt + timedelta(hours=1):
		if eml_dt < recv_dt - timedelta(days=15) or \
			eml_dt > recv_dt + timedelta(days=2):
			score += 2
			log += 'far time '
			put(red("time") + " %s " % str(recv_dt - eml_dt))
		else:
			score += 1
			log += 'near time '
			put(yel("time") + " %s " % str(recv_dt - eml_dt))
	if eml.get('X-Spam-Status', '').lower() == 'yes' or \
		eml.get('X-Spam-Flag', '').lower() == 'yes' or \
		len(eml.get('X-Spam-Level', '')) > 2:
		score += 1
		log += 'X-Spam '
		put(yel("X-Spam "))
	put('\033[1;35m%s\033[0m\n' % score)  # purple
	return score, eml, log


bad_chars_re = compile_re(r'[ >\n\xc2\xa0.,@#-=:*\]\[+_()/|\'\t\r\f\v]')
is_alpha_re = compile_re(r'[^\u0040-\u024F]')


def email_alpha_len(t: str, f: typing.Callable[[str], str]) -> tuple[int, int]:
	try:
		s = f(t)
	except UnicodeDecodeError as e:
		put(str(e) + '\n')
		s = t
	except Exception as e:
		put(str(e) + '\n')
		s = ''
	s_len = len(s)
	s_with_bad_chars_len = len(s)
	s = bad_chars_re.sub('', s)
	bad_chars_len = s_with_bad_chars_len - len(s)
	# ascii_s = s.encode('ascii', errors='ignore')
	# # s_unicode_len = len([c for c in s if s.isalnum()])  # considers chinese char as alpha
	# s_alpha_len = len([c for c in ascii_s if isalpha(c)])
	# print(s)
	# print(is_alpha_re.sub('', s))
	s_alpha_len = len(is_alpha_re.sub('', s))
	# print('len() ' + str(s_alpha_len))
	return s_len - bad_chars_len, s_alpha_len


txt_links_re = compile_re(r'http.?://([^./]*?\.)*([^./]*?\.[^./]*?)[/ \n]')
htm_links_re = compile_re(r'href=.http.?://([^./]*?\.)*([^./]*?\.[^./]*?)[/ "\n]')
txt_linkwords_re = compile_re(r'(unsubscribe|click|sinscr|abonnement|lien)', IGNORECASE)
too_big_words_re = compile_re(r'[^/\-\s]{26,}')


def max_same_links(t: str, links_re: Pattern) -> int:
	domains = [a[1] for a in links_re.findall(str(t))]  # list of domain parts, ignoring subdom.
	occurences = [domains.count(a) for a in domains]
	occurences.sort()
	return occurences[-1] if len(occurences) else 0


def header_str(h: str) -> str:
	return str(make_header(decode_header(h)))


def same_links_score(max_same_links: int, name: str) -> tuple[int, str]:
	score = 0
	log = ''
	if max_same_links > 6:
		if max_same_links > 20:
			score += 2
			put('same %s ' % name + red("links") + " %i " % max_same_links)
		else:
			score += 1
			put('same %s ' % name + yel("links") + " %i " % max_same_links)
		log += 'same %s links %i ' % (name, max_same_links)
	return score, log


def too_big_words_score(t: str) -> tuple[int, str]:
	score = 0
	log = ''
	too_big_words = len([a for a in too_big_words_re.findall(str(t))])
	if too_big_words > 4:
		if too_big_words > 10:
			score += 2
			put(' %i too big ' % too_big_words + red("words"))
		else:
			score += 1
			put(' %i too big ' % too_big_words + yel("words"))
		log += '%i too big words ' % too_big_words
	return score, log


def txt_alpha_len(txt_len: int, txt_alpha_len: int, name: str, min_len: int) -> tuple[int, str]:
	score = 0
	log = ''
	if txt_alpha_len == 0 or txt_len // txt_alpha_len > 1:  # too small, not so interesting
		score += 1
		log += 'tiny %s %i/%i ' % (name, txt_alpha_len, txt_len)
		put(yel("tiny") + " %s %i/%i " % (name, txt_alpha_len, txt_len))
		if txt_alpha_len < min_len:
			score += 1
			log += '< %ic ' % min_len
			put('<%ic ' % min_len)
		if txt_alpha_len > 0 and txt_len // txt_alpha_len > 4:
			score += 1
			log += '< 25% '
			put(red('<25% '))
	return score, log


def yel(s: str) -> str: return '\033[1;33m' + s + '\033[0m'  # noqa
def red(s: str) -> str: return '\033[1;31m' + s + '\033[0m'  # noqa
def put(s: str): __debug__ and print(s, end='', file=sys.stderr)  # noqa
# def put(s: str): print(s, end='', file=sys.stderr)  # noqa
